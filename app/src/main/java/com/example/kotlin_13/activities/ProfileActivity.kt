package com.example.kotlin_13.activities


import android.app.AlertDialog
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.bumptech.glide.Glide
import com.example.kotlin_13.activities.data.FirebaseClient
import com.example.kotlin_13.databinding.ActivityProfileBinding
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import android.provider.MediaStore
import android.util.TypedValue
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.kotlin_13.activities.caching.UserPreferences
import com.example.kotlin_13.activities.connection.ConnectionMessageSingleton
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver


class ProfileActivity : AppCompatActivity() {

    private val firebaseAuth = FirebaseClient.getAuthInstance()

    private lateinit var builder: AlertDialog.Builder

    private var imageUri: Uri? = null

    private val db = FirebaseClient.getDatabase()

    private var storageRef = FirebaseClient.getStorageRef()

    private val connectionMessages= ConnectionMessageSingleton

    private var internet = true

    private var uriPend: Uri? = null


    // Crea un método para iniciar la cámara
    private var cameraActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult( ActivityResultContracts.StartActivityForResult()) { result ->
            if(result != null){
                if(internet){
                    //case there is internet connection
                    uploadImage(imageUri)
                }
                else{
                    uriPend = imageUri
                }

            }
        }

        // Crea un archivo para guardar la imagen capturada

    private var imagePickerActivityResult:  ActivityResultLauncher<Intent> =
    // lambda expression to receive a result back, here we
        // receive single item(photo) on selection
        registerForActivityResult( ActivityResultContracts.StartActivityForResult()) { result ->
            if (result != null) {
                // getting URI of selected Image
                val imageUri: Uri? = result.data?.data
                if(internet){
                    uploadImage(imageUri)
                }
                else{
                    uriPend = imageUri
                }
            }
        }

    private fun uploadImage(imageUri: Uri?) {

        // Try to catch NullPointerException if a photo is not selected
        try{

            // Get the userId to name the image file
            val userId = firebaseAuth.currentUser!!.uid

            // Name of the file will be userId
            val uploadTask = storageRef.child("profile_photos/$userId").putFile(imageUri!!)

            // On success, download the file URL and display it
            uploadTask.addOnSuccessListener {
                // Glide library to display the image
                storageRef.child("profile_photos/$userId").downloadUrl.addOnSuccessListener {
                    Glide.with(this@ProfileActivity)
                        .load(it)
                        .into(binding.imageView)

                    // Get the current user to update photo in auth Firebase
                    val user = FirebaseAuth.getInstance().currentUser

                    // Update photo in auth user
                    val profileUpdate = userProfileChangeRequest {
                        photoUri = it

                    }

                    // Update the photo in the Firestore db
                    val updateMap = mapOf(
                        "photo" to it.toString()
                    )
                    val userId = user!!.uid
                    db.collection("clients").document(userId).update(updateMap)
                    UserPreferences.setPhoto(it.toString())

                    Log.e("Firebase", "download passed")
                }.addOnFailureListener {
                    Log.e("Firebase", "Failed in downloading")
                }
            }.addOnFailureListener {
                Log.e("Firebase", "Image Upload fail")
            }
        }
        catch (e: java.lang.NullPointerException){
            Toast.makeText(this, "No picture selected", Toast.LENGTH_SHORT).show()
        }



    }


    private lateinit var binding: ActivityProfileBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        builder = AlertDialog.Builder(this)

        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if(isConnected){
                if(connectionMessages.getShowProfile() === true){

                    if(uriPend != null){
                        uploadImage(uriPend)
                        uriPend = null
                    }
                    var nameChange = UserPreferences.getUserName()
                    var emailChange = UserPreferences.getUserEmail()


                    builder.setMessage("Internet is back!")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            connectionMessages.restartShowProfile()


                            if (nameChange != null) {
                                if (emailChange != null) {
                                    uploadData(nameChange,emailChange)
                                }
                            }
                            //recreate()
                            dialogInterface.cancel()

                        }
                        .show()
                    internet = true


                }

            }
            else{
                if(connectionMessages.getShowProfile() === false){
                    builder.setMessage("No Internet connection. Updates will save once the internet is back")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            dialogInterface.cancel()
                        }
                        .show()
                    internet = false
                    connectionMessages.showProfile()

                }


            }
        })


        binding.btnMenus.setOnClickListener{
            val intent = Intent(this, MenuListActivity::class.java)
            startActivity(intent)
        }

        // Configura el oyente de clics para el botón utilizando ViewBinding
        binding.buttonChangePhoto.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Select Image")
                .setMessage("Choose your option")
                .setPositiveButton("Gallery"){ dialog, _ ->
                    dialog.dismiss()

                    // Action for picking an element and return what was selected
                    val galleryIntent = Intent(Intent.ACTION_PICK)
                    // The type of the choice is image
                    galleryIntent.type = "image/*"

                    // Process of selecting and updating the photo
                    imagePickerActivityResult.launch(galleryIntent)


                }
                .setNegativeButton("Camera"){ dialog, _ ->
                    dialog.dismiss()



                    //checkPermissionsAndDispatchTakePictureIntent()
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also{
                        val permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                        if (permission != PackageManager.PERMISSION_GRANTED){
                            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 1)

                        }
                        else{
                                val values = ContentValues()
                                imageUri = contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

                                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                                cameraActivityResult.launch(cameraIntent)



                        }
                    }



                }
                .show()
        }

        val buttonChangePicture = binding.buttonChangePhoto

        val maxSize = 10

        val scaledSizePixels = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            maxSize.toFloat(),
            resources.displayMetrics
        )

        buttonChangePicture.setTextSize(TypedValue.COMPLEX_UNIT_PX, scaledSizePixels)




        // Button to go back to MainActivity
        binding.backBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        // minLength name
        val editText = binding.nameET
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // do nothing
            }

            override fun afterTextChanged(s: Editable) {
                if (s.length < 2) {
                    editText.error = "Input must be at least 2 characters"
                }
            }
        })


        // Button for updating name, email, and password
        binding.btnUpdate.setOnClickListener {

            val name = binding.nameET.text.toString()
            val email = binding.emailET.text.toString()

            if(internet){
                uploadData(name, email)
            }
            else{
                saveDataPreferences(name,email)
                builder.setMessage("Your changes will save once the internet connection is back")
                    .setPositiveButton("OK"){dialogInterface,it ->
                        dialogInterface.cancel()
                    }
                    .show()
            }


        }


    }

    private fun saveDataPreferences(name: String, email: String) {
        UserPreferences.setName(name)
        UserPreferences.setEmail(email)
    }

    private fun uploadData(name: String, email: String) {

        saveDataPreferences(name, email)

        val userId = firebaseAuth.currentUser!!.uid
        val ref = db.collection("clients").document(userId)

        ref.get().addOnSuccessListener {
            if(it != null){

                // Get current email and password for credentials
                val currentPassWord =it.data?.get("password").toString()
                val currentEmail = it.data?.get("email").toString()

                // Update in Auth with credentials current email and current password
                updateUser(currentEmail, name, currentPassWord,email)

                // Update in firebase db
                val updateMap = mapOf(
                    "name" to name,
                    "email" to email
                )

                val userId = firebaseAuth.currentUser!!.uid
                db.collection("clients").document(userId).update(updateMap)

            }
        }

        builder.setMessage("Data was updated")
            .setPositiveButton("OK"){dialogInterface,it ->
                dialogInterface.cancel()
            }
            .show()
        // Redirects to MainActivity
        //val intent = Intent(this, MainActivity::class.java)
        //startActivity(intent)
    }

    override fun onStart() {
        super.onStart()

        // On start, predetermined values should appear in the fields, obtained from the db
        val nameEt = binding.nameET
        val emailET = binding.emailET
        val statusText = binding.statusTxt
        val photo = binding.imageView

        // Get the reference of the document of the current user
        val userId = firebaseAuth.currentUser!!.uid
        val ref = db.collection("clients").document(userId)

        ref.get().addOnSuccessListener {
            if(it != null){


                val userId = UserPreferences.getUserId()
                val currentName = UserPreferences.getUserName()
                val currentEmail = UserPreferences.getUserEmail()
                val currentStatus = UserPreferences.getUserStatus()
                val currentPhoto = UserPreferences.getUserPhoto()



                // To set the default values on the fields
                statusText.text = currentStatus
                nameEt.setText(currentName)
                emailET.setText(currentEmail)

                // Glide library to display the profile photo
                Glide.with(this@ProfileActivity)
                    .load(currentPhoto)
                    .into(photo)

            }
        }
            .addOnFailureListener{
                Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
            }
    }

    private fun updateUser(email:String, name:String, password:String, newEmail:String){
        val user = FirebaseAuth.getInstance().currentUser

        // Update name
        val profileUpdate = userProfileChangeRequest {
            displayName = name

        }

        // Get credential
        val credential = EmailAuthProvider
            .getCredential(email, password)

        user!!.reauthenticate(credential)
            .addOnCompleteListener {
                Log.d(TAG, "User re-authenticated.")

                //Update email
                user!!.updateEmail(newEmail)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "User email address updated.")
                        }
                    }
                    .addOnCanceledListener {

                    }
                //----------------------------------------------------------\\
            }
    }
}