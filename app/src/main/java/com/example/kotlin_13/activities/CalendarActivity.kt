package com.example.kotlin_13.activities


import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.kotlin_13.R
import java.util.Calendar
import com.example.kotlin_13.databinding.ActivityCalendarBinding
import com.example.kotlin_13.viewmodels.ViewModelCalendar

class CalendarActivity : AppCompatActivity() {

    private lateinit var datePicker: DatePicker
    private lateinit var timePicker: TimePicker
    private lateinit var payButton: Button
    private lateinit var binding: ActivityCalendarBinding
    private lateinit var viewModel: ViewModelCalendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalendarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtenemos las referencias de los elementos del layout
        datePicker = findViewById(R.id.date_picker)
        timePicker = findViewById(R.id.time_picker)
        payButton = findViewById(R.id.pay_button)

        viewModel = ViewModelProvider(this).get(ViewModelCalendar ::class.java)

        // Asignamos el listener para el botón de pago
        payButton.setOnClickListener {
            // Obtenemos la fecha y hora seleccionadas
            val year = datePicker.year
            val month = datePicker.month
            val day = datePicker.dayOfMonth
            val hour = timePicker.hour
            val minute = timePicker.minute

            // Creamos un objeto Calendar con la fecha y hora seleccionadas
            val calendar = Calendar.getInstance()
            calendar.set(year, month, day, hour, minute)

            // Realizamos la operación correspondiente
            viewModel.realizarPago(calendar)
        }

        // Button to go back to PaymentActivity
        binding.backBtn.setOnClickListener {
            viewModel.volverAPaymentActivity(this)
        }
    }
}
