package com.example.kotlin_13.activities

import javax.inject.Singleton

@Singleton
object Counter {
    private var value: Int = 0

    fun increment() {
        value++
    }

    fun restart(){
        value = 0
    }

    fun getValue(): Int {
        return value
    }
}