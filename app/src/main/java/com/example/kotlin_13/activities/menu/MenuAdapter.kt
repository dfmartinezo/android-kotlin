import android.content.ClipData
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlin_13.R
import com.example.kotlin_13.activities.CircleTransform
import com.example.kotlin_13.activities.OrderMenuActivity
import com.example.kotlin_13.activities.menu.MenuItem

class MenuAdapter(private val menuList: ArrayList<MenuItem>) :
    RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val menuName: TextView = itemView.findViewById(R.id.menu_name)
        //val menuDescription: TextView = itemView.findViewById(R.id.menu_description)
        val menuPrice: TextView = itemView.findViewById(R.id.menu_price)
        val menuImage: ImageView = itemView.findViewById(R.id.menu_image)

        fun bind(item: ArrayList<String>) {

            itemView.setOnClickListener {
                val context = itemView.context
                val intent = Intent(context, OrderMenuActivity::class.java)
                intent.putExtra("selectedItem", item)
                context.startActivity(intent)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_item, parent, false)
        return MenuViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val currentItem = menuList[position]

        val arrayList = arrayListOf(currentItem.name,currentItem.photoURL)

        holder.bind(arrayList)

        holder.menuName.text = currentItem.name
        //holder.menuDescription.text = currentItem.description
        holder.menuPrice.text = currentItem.price.toString()
        Glide.with(holder.itemView.context)
            .load(currentItem.photoURL)
            .transform(CircleTransform())
            .placeholder(R.drawable.default_)
            .into(holder.menuImage)



    }

    override fun getItemCount(): Int {
        return menuList.size
    }
}
