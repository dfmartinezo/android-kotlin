package com.example.kotlin_13.activities

import MenuAdapter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_13.R
import com.example.kotlin_13.databinding.ActivityMenuListLoginSignupBinding
import com.example.kotlin_13.activities.menu.MenuItem
import java.io.*

class MenuListLoginSignUpActivity: AppCompatActivity() {
    private lateinit var binding: ActivityMenuListLoginSignupBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_list_login_signup)
        binding = ActivityMenuListLoginSignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myRecyclerView = binding.recyclerView

        var menuList = readMenuListFromDisk()
        val adapter = MenuAdapter(menuList)
        myRecyclerView.adapter = adapter




        binding.btnBack.setOnClickListener {
            finish()
        }

    }

    private fun readMenuListFromDisk(): ArrayList<MenuItem> {
        var menuList = arrayListOf<MenuItem>()
        menuList.clear()
        try {
            val file = File(getExternalFilesDir(null), "menu_list.dat")
            val fis = FileInputStream(file)
            val ois = ObjectInputStream(fis)
            menuList = ois.readObject() as ArrayList<MenuItem>
            ois.close()
            fis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        return menuList
    }
}
