package com.example.kotlin_13.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import MenuAdapter
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.R
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import com.example.kotlin_13.databinding.ActivityMenuListBinding
import com.example.kotlin_13.activities.menu.MenuItem
import com.google.firebase.firestore.FirebaseFirestore
import java.io.*

class MenuListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMenuListBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var menuAdapter: MenuAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_list)
        binding = ActivityMenuListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myRecyclerView = findViewById<RecyclerView>(R.id.recycler_view)

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if (isConnected) {
                val db = FirebaseFirestore.getInstance()
                val collectionRef = db.collection("menus")
                collectionRef.get().addOnSuccessListener { querySnapshot ->
                    val dataList = ArrayList<MenuItem>()

                    for (document in querySnapshot.documents) {
                        val data = document.toObject(MenuItem::class.java)
                        data?.let {
                            dataList.add(it)
                        }
                    }

                    // Guarda la lista de menús en el almacenamiento local
                    saveMenuListToDisk(dataList)

                    val adapter = MenuAdapter(dataList)
                    myRecyclerView.adapter = adapter
                }

            } else {

                var menuList = readMenuListFromDisk()
                val adapter = MenuAdapter(menuList)
                myRecyclerView.adapter = adapter

            }
        })

        binding.btnProfile.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }


        // Button to go back to MainActivity
        binding.backBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }




    }

    private fun readMenuListFromDisk(): ArrayList<MenuItem> {
        var menuList = arrayListOf<MenuItem>()
        menuList.clear()
        try {
            val file = File(getExternalFilesDir(null), "menu_list.dat")
            val fis = FileInputStream(file)
            val ois = ObjectInputStream(fis)
            menuList = ois.readObject() as ArrayList<MenuItem>
            ois.close()
            fis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        return menuList
    }

    private fun saveMenuListToDisk(menuList: ArrayList<MenuItem>) {
        try {
            val file = File(getExternalFilesDir(null), "menu_list.dat")
            val fos = FileOutputStream(file)
            val oos = ObjectOutputStream(fos)
            oos.writeObject(menuList)
            oos.close()
            fos.close()
            Log.d("TAG", "Menus saved locally")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
