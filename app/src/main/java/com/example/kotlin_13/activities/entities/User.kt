package com.example.kotlin_13.activities.entities

data class User(
    val id: String,
    val name: String,
    val email: String,
    val status: String,
    val photo: String
)