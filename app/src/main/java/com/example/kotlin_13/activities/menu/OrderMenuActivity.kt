package com.example.kotlin_13.activities


import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.bumptech.glide.Glide
import com.example.kotlin_13.activities.data.FirebaseClient
import com.example.kotlin_13.databinding.OrderMenuBinding
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import android.provider.MediaStore
import android.util.TypedValue
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isNotEmpty
import com.example.kotlin_13.R
import com.example.kotlin_13.activities.caching.UserPreferences
import com.example.kotlin_13.activities.connection.ConnectionMessageSingleton
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import java.text.SimpleDateFormat
import java.util.*


class OrderMenuActivity : AppCompatActivity() {

    private lateinit var builder: AlertDialog.Builder

    private val db = FirebaseClient.getDatabase()

    private lateinit var binding: OrderMenuBinding

    private val connectionMessages= ConnectionMessageSingleton

    private var day = 0

    private var month = 0

    private var todayDate = ""

    private var internet = true

    private var menuName: ArrayList<String> = ArrayList()




    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        menuName = intent.getSerializableExtra("selectedItem") as ArrayList<String>

        builder = AlertDialog.Builder(this)

        super.onCreate(savedInstanceState)
        binding = OrderMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val photo = binding.imageView

        Glide.with(this@OrderMenuActivity)
            .load(menuName[1])
            .into(photo)



        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if (isConnected) {
                Log.d("Connection", connectionMessages.getShowOrder().toString())

                if (connectionMessages.getShowOrder() === true) {

                    var hourPend = UserPreferences.getUserHour()
                    var monthPend = UserPreferences.getUserMonth()
                    var dayPend = UserPreferences.getUserDay()


                    builder.setMessage("Internet is back!")
                        .setPositiveButton("OK") { dialogInterface, it ->
                            connectionMessages.restartShowOrder()

                            if (hourPend !=null ) {
                                if (monthPend != null) {
                                    if(dayPend != null){
                                        uploadData(monthPend,dayPend,hourPend)

                                    }
                                }
                            }
                            dialogInterface.cancel()


                        }
                        .show()
                    internet =true

                }
            }
            else {
                if (connectionMessages.getShowOrder() === false) {
                    builder.setMessage("No Internet connection. Updates will save once the internet is back")
                        .setPositiveButton("OK") { dialogInterface, it ->

                            dialogInterface.cancel()
                        }
                        .show()

                    connectionMessages.showOrder()

                    internet =false
                }



            }
        })

        // Date picker
        binding.btnSelectDate.setOnClickListener{
            val c = Calendar.getInstance()

            // on below line we are getting
            // our day, month and year.
            val year = c.get(Calendar.YEAR)
            month = c.get(Calendar.MONTH) +1
            day = c.get(Calendar.DAY_OF_MONTH)

            // on below line we are creating a
            // variable for date picker dialog.
            val datePickerDialog = DatePickerDialog(
                // on below line we are passing context.
                this, R.style.DialogTheme,
                { view, year, monthOfYear, dayOfMonth ->
                    // on below line we are setting
                    // date to our text view.
                    var selected =
                        (year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth)
                    binding.txtDate.text=selected

                    month = monthOfYear+1
                    day = dayOfMonth


                },
                // on below line we are passing year, month
                // and day for the selected date in our date picker.
                year,
                month,
                day
            )

            val maxYear = 2023
            val maxMonth = Calendar.DECEMBER
            val maxDay = 31

            val maxCalendar = Calendar.getInstance().apply {
                set(maxYear, maxMonth, maxDay)
            }

            datePickerDialog.datePicker.apply {
                maxDate = maxCalendar.timeInMillis
                
            }

            val dateToday = Calendar.getInstance()
            todayDate = dateToday.get(Calendar.YEAR).toString()+"/"+(dateToday.get(Calendar.MONTH) + 1).toString()+"/"+dateToday.get(Calendar.DAY_OF_MONTH).toString()
            datePickerDialog.datePicker.minDate = dateToday.timeInMillis;
            // at last we are calling show
            // to display our date picker dialog.
            datePickerDialog.show()


        }

        // Button to go back to MainActivity
        binding.backBtn.setOnClickListener {
            val intent = Intent(this, MenuListActivity::class.java)
            startActivity(intent)
        }

        // Button for updating name, email, and password
        binding.btnOrder.setOnClickListener {

            val hour = binding.spinner.selectedItem

            if((hour !=null) and (month >0) and (day>0)){
                if(internet){
                    uploadData(month.toString(), day.toString(), hour.toString())
                }
                else{
                    saveDataPreferences(month.toString(), day.toString(), hour.toString())
                    builder.setMessage("Your changes will save once the internet connection is back")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            dialogInterface.cancel()
                        }
                        .show()
                }
            }
            else{
                builder.setMessage("Empty spaces are not permitted")
                    .setPositiveButton("OK"){dialogInterface,it ->
                        dialogInterface.cancel()
                    }
                    .show()
            }




        }


    }

    private fun saveDataPreferences(month:String, day:String, hour:String) {
        UserPreferences.setMonth(month)
        UserPreferences.setDay(day)
        UserPreferences.setHour(hour)

    }

    private fun uploadData(month:String, day:String, hour:String) {


        saveDataPreferences(month,day,hour)


        val orderMap = hashMapOf(
            "creationDate" to todayDate,
            "deliverDay" to day,
            "deliverHour" to hour,
            "deliverMonth" to month,
            "menuRef" to menuName[0],
        )
        db.collection("ordenes").document(generateRandomNumber().toString()).set(orderMap).addOnCompleteListener { dbTask ->
            builder.setMessage("Order generated!")
                .setPositiveButton("OK"){dialogInterface,it ->

                    UserPreferences.deleteOrder()
                    val intent = Intent(this, MenuListActivity::class.java)
                    startActivity(intent)
                }
                .show()
        }

    }

    private fun generateRandomNumber(): Long {
        val random = Random()
        val min = 1_000_000_000L
        val max = 10_000_000_000L
        return random.nextInt((max - min).toInt()) + min
    }


}