package com.example.kotlin_13.activities.bottomNavigation

import com.example.kotlin_13.R

sealed class ItemsMenu(
    val icon: Int,
    val title: String,
    val ruta: String
) {
    object Screen1: ItemsMenu(
        R.drawable.baseline_emoji_people_24,
        "Profile", "Screen1"
    )
    object Screen2: ItemsMenu(
        R.drawable.baseline_home_24,
        "Home", "Screen2"
    )
    object Screen3: ItemsMenu(
        R.drawable.baseline_menu_book_24,
        "Menu", "Screen3"
    )
}
