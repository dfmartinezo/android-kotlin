package com.example.kotlin_13.activities.caching

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import com.example.kotlin_13.activities.entities.User
import javax.inject.Singleton

@Singleton
object UserPreferences : SharedPreferences.OnSharedPreferenceChangeListener {

    private const val PREF_NAME = "UserPreferences"
    private const val USER_ID = "user_id"
    private const val USER_NAME = "user_name"
    private const val USER_EMAIL = "user_email"
    private const val USER_STATUS = "user_status"
    private const val USER_PHOTO = "user_photo"
    private const val USER_MONTH = "user_month"
    private const val USER_DAY = "user_day"
    private const val USER_HOUR = "user_hour"
    private const val USER_URI = "user_uri"

    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        preferences.registerOnSharedPreferenceChangeListener(this)
    }

    fun saveUserData(user: User) {
        with(preferences.edit()) {
            putString(USER_ID, user.id)
            putString(USER_NAME, user.name)
            putString(USER_EMAIL, user.email)
            putString(USER_STATUS, user.status)
            putString(USER_PHOTO, user.photo)
            apply()
        }
    }

    fun deleteData(){
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

    fun deleteOrder(){
        val elementosABorrar = listOf(USER_MONTH, USER_DAY, USER_HOUR)
        val editor = preferences.edit()
        for (elemento in elementosABorrar) {
            editor.remove(elemento)
        }
        editor.apply()
    }


    fun getUserId(): String? {
        return preferences.getString(USER_ID, null)
    }

    fun setName(name:String){
        with(preferences.edit()) {
            putString(USER_NAME, name)
            apply()
        }
    }

    fun getUserName(): String? {
        return preferences.getString(USER_NAME, null)
    }

    fun setEmail(email:String){
        with(preferences.edit()) {
            putString(USER_EMAIL, email)
            apply()
        }
    }
    fun getUserEmail(): String? {
        return preferences.getString(USER_EMAIL, null)
    }

    fun getUserStatus(): String? {
        return preferences.getString(USER_STATUS, null)
    }

    fun setPhoto(url:String){
        with(preferences.edit()) {
            putString(USER_PHOTO, url)
            apply()
        }
    }
    fun getUserPhoto(): String? {
        return preferences.getString(USER_PHOTO, null)
    }

    fun setMonth(month:String){
        with(preferences.edit()) {
            putString(USER_MONTH, month)
            apply()
        }
    }
    fun getUserMonth(): String? {
        return preferences.getString(USER_MONTH, null)
    }

    fun setDay(day:String){
        with(preferences.edit()) {
            putString(USER_DAY, day)
            apply()
        }
    }
    fun getUserDay(): String? {
        return preferences.getString(USER_DAY, null)
    }

    fun setHour(hour:String){
        with(preferences.edit()) {
            putString(USER_HOUR, hour)
            apply()
        }
    }
    fun getUserHour(): String? {
        return preferences.getString(USER_HOUR, null)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        // Handle changes in SharedPreferences data here
    }
}