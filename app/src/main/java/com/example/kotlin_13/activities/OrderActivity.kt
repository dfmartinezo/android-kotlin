package com.example.kotlin_13.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.R
import com.example.kotlin_13.databinding.ActivityOrderBinding

class OrderActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOrderBinding


    override fun onCreate(savedInstanceState: Bundle?) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_menu_list)
            binding = ActivityOrderBinding.inflate(layoutInflater)
            setContentView(binding.root)


        binding.backBtn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }



}