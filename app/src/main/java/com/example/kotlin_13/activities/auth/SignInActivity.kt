package com.example.kotlin_13.activities.auth

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.activities.MainActivity
import com.example.kotlin_13.activities.MenuListLoginSignUpActivity
import com.example.kotlin_13.activities.connection.ConnectionMessageSingleton
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import com.example.kotlin_13.activities.data.FirebaseClient
import com.google.firebase.auth.FirebaseAuth
import com.example.kotlin_13.databinding.ActivityLoginBinding
import com.example.kotlin_13.activities.menu.MenuItem
import com.example.kotlin_13.model.repositories.UserRepository
import com.example.kotlin_13.viewmodels.UserViewModel
import com.google.firebase.firestore.FirebaseFirestore
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.ObjectOutputStream

class SignInActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var builder: AlertDialog.Builder

    private val userRepository = UserRepository()
    private val viewModel = UserViewModel(userRepository)

    private val connectionMessages= ConnectionMessageSingleton


    private fun saveMenuListToDisk(menuList: ArrayList<MenuItem>) {
        try {
            val file = File(getExternalFilesDir(null), "menu_list.dat")
            val fos = FileOutputStream(file)
            val oos = ObjectOutputStream(fos)
            oos.writeObject(menuList)
            oos.close()
            fos.close()
            Log.d("TAG", "Menus saved locally")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        builder = AlertDialog.Builder(this)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Auth service
        firebaseAuth = FirebaseClient.getAuthInstance()

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if(isConnected){
                val db = FirebaseFirestore.getInstance()
                val collectionRef = db.collection("menus")
                collectionRef.get().addOnSuccessListener { querySnapshot ->
                    val dataList = ArrayList<MenuItem>()

                    for (document in querySnapshot.documents) {
                        val data = document.toObject(MenuItem::class.java)
                        data?.let {
                            dataList.add(it)
                        }
                    }

                    // Guarda la lista de menús en el almacenamiento local
                    saveMenuListToDisk(dataList)
                if(connectionMessages.getShowSignIn() === true){
                    if(firebaseAuth.currentUser != null){
                        firebaseAuth.signOut()
                    }
                    builder.setMessage("Internet is back!")
                        .setPositiveButton("Refresh"){dialogInterface,it ->
                            connectionMessages.restartShowSignIn()

                            recreate()
                        }
                        .show()

                }

                // If user is logged, goes directly to MainActivity
                if(firebaseAuth.currentUser != null){
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }

            }
                }
            else{

                if(connectionMessages.getShowSignIn() === false){
                    builder.setMessage("No Internet connection. Login once the connection is back")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            dialogInterface.cancel()
                        }
                        .show()
                    connectionMessages.showSignIn()

                }

                binding.btnLogin.isEnabled = false
                binding.btnLogin.setBackgroundColor(Color.GRAY)
            }
        })

        viewModel.isSignInSuccessful.observe(this) { isSuccess ->
            if (isSuccess) {
                // Redirects to signInActivity, but because it is logged, it goes to MainActivity
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                // El registro falló, realiza alguna acción aquí
                builder.setMessage("Incorrect credentials")
                    .setPositiveButton("Cancel"){dialogInterface,it ->
                        val intent = Intent(this, SignInActivity::class.java)
                        startActivity(intent)                    }
                    .show()
            }
        }

        // Register Button, opens SignUpActivity
        binding.registerButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        binding.menus.setOnClickListener {
            val intent = Intent(this, MenuListLoginSignUpActivity::class.java)
            startActivity(intent)
        }

        // Login Button when email and password spaces are filled
        binding.btnLogin.setOnClickListener {
            val email = binding.emailEt.text.toString()
            val pass = binding.passET.text.toString()

            viewModel.signIn(email,pass,builder)


        }
    }

}