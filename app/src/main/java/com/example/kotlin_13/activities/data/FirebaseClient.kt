package com.example.kotlin_13.activities.data

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import javax.inject.Singleton

@Singleton
object FirebaseClient  {
    private val firebaseAuth : FirebaseAuth = FirebaseAuth.getInstance()
    private val db = Firebase.firestore
    private val storageRef = Firebase.storage.reference

    fun getAuthInstance(): FirebaseAuth {
        return firebaseAuth
    }

    fun getDatabase(): FirebaseFirestore {
        return db
    }

    fun getStorageRef(): StorageReference {
        return storageRef
    }

}