package com.example.kotlin_13.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.kotlin_13.databinding.ActivityPaymentBinding
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.kotlin_13.R


import java.util.concurrent.Executor

class PaymentActivity : AppCompatActivity() {
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private lateinit var payWithCardButton: Button
    private lateinit var binding: ActivityPaymentBinding









    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        binding = ActivityPaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val radioGroup: RadioGroup = findViewById(R.id.payment_radio_group)
        val creditCardLayout: LinearLayout = findViewById(R.id.credit_card_info_layout)

        payWithCardButton = findViewById(R.id.pay_with_card_button)
        val radioButton1: RadioButton = findViewById(R.id.radio_cash)
        val radioButton2: RadioButton = findViewById(R.id.radio_credit_card)
        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radio_credit_card -> {
                    creditCardLayout.visibility = View.VISIBLE
                    showBiometricPrompt()
                }
                R.id.radio_cash -> {
                    creditCardLayout.visibility = View.GONE
                    payWithCardButton.isEnabled = false
                }
            }
        }



        setupBiometricPrompt()
        // Button to go back to MainActivity
        binding.backBtn.setOnClickListener {
            val intent = Intent(this, MenuListActivity::class.java)
            startActivity(intent)
        }



        // Button to go to calendar
        binding.CalendarButton.setOnClickListener {
            val intent = Intent(this, CalendarActivity::class.java)

            val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val network = connectivityManager.activeNetwork
            val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
            if (networkCapabilities == null || !networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                val builder = AlertDialog.Builder(this)
                builder.setMessage("No se puede procesar el pago, no hay conexión a internet")
                builder.setPositiveButton("Ok") { dialog, which ->
                    dialog.dismiss()
                }
                val dialog = builder.create()
                dialog.show()
            }

            else if (!radioButton1.isChecked() && !radioButton2.isChecked() ) {
                val builder = AlertDialog.Builder(this)
                builder.setMessage("Elija una de las opciones")
                builder.setPositiveButton("Ok") { dialog, which ->
                    dialog.dismiss()
                }
                val dialog = builder.create()
                dialog.show()
            }


             else {
                startActivity(intent)
            }
        }





    }

    private fun setupBiometricPrompt() {
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                // Handle the error
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                // Handle successful authentication
                payWithCardButton.isEnabled = true
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                // Handle the failed authentication
            }
        })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.biometric_prompt_title))
            .setSubtitle(getString(R.string.biometric_prompt_subtitle))
            .setNegativeButtonText(getString(R.string.biometric_prompt_negative_button_text))
            .build()
    }

    private fun showBiometricPrompt() {
        biometricPrompt.authenticate(promptInfo)
    }




}
