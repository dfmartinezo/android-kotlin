package com.example.kotlin_13.activities

import MenuAdapter
import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_13.R
import com.example.kotlin_13.activities.auth.SignInActivity
import com.example.kotlin_13.activities.caching.UserPreferences
import com.example.kotlin_13.activities.connection.ConnectionMessageSingleton
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import com.example.kotlin_13.activities.data.FirebaseClient
import com.example.kotlin_13.databinding.ActivityMainBinding
import com.example.kotlin_13.activities.entities.User
import com.google.android.gms.location.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONObject
import java.net.URL


class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAuth: FirebaseAuth

    // private lateinit var firebaseClient : FirebaseClient
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var recyclerView: RecyclerView
    private lateinit var menuAdapter: MenuAdapter

    private lateinit var builder: AlertDialog.Builder

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var latitude:Double = 0.0
    private var longitude:Double = 0.0

    var temp = 0.0


    private var nearUniversity:Boolean =false



    private var contador = Counter

    private val connectionMessages= ConnectionMessageSingleton


    //private var builder= AlertDialog.Builder(this)

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)



        builder = AlertDialog.Builder(this)


        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        UserPreferences.init(this)


        binding.logoutBtn.setOnClickListener {
            firebaseAuth.signOut()
            UserPreferences.deleteData()
            Counter.restart()
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }


        bottomNavigation = findViewById(R.id.bottom_navigation)
        bottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_home -> {
                    //val intent = Intent(this, PaymentActivity::class.java)
                    //startActivity(intent)
                }
                R.id.nav_menu -> {
                    // Code to navigate to menu screen
                    // Menu List
                    val intent = Intent(this, MenuListActivity::class.java)
                    startActivity(intent)
                }
                R.id.nav_profile -> {
                    // Code to navigate to profile screen
                    val intent = Intent(this, ProfileActivity::class.java)
                    startActivity(intent)
                }
            }
            true
        }

    }

    private fun getCurrentLocation(){

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission()
            return
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        if(checkPermissions()){
            if(isLocationEnabled()){

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermission()
                    return
                }
                fusedLocationProviderClient.lastLocation.addOnCompleteListener(this){ task ->
                    val location: Location?= task.result
                    if(location == null){
                        Toast.makeText(this,"null recieved", Toast.LENGTH_SHORT).show()
                    }
                    else{
                        latitude = location.latitude
                        longitude = location.longitude

                        nearUniversity = checkNearUniversity()

                        if(temp > 20 && Counter.getValue() == 0 && nearUniversity){

                            Log.d("Debug", "Temperatura: $temp, Contador: ${Counter.getValue()}, Near: $nearUniversity")

                            Counter.increment()

                            showDialogView()


                        }
                        else{
                            Log.d("Debug", "Temperatura: $temp, Contador: ${Counter.getValue()}, Near: $nearUniversity")
                        }

                        //binding.tempText.text = "$latitude, $longitude"


                    }
                }
            }
            else{
                // setting open
                builder.setMessage("Turn on location")
                    .setPositiveButton("OK"){dialogInterface,it ->
                        dialogInterface.cancel()
                    }
                    .show()

                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)

                startActivity(intent)

            }



        }
        else{
            // request permission
            requestPermission()

        }
    }

    private fun isLocationEnabled():Boolean{
        val locationManager: LocationManager=getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf( android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_ACCESS_LOCATION
        )
    }


    companion object{
        private const val PERMISSION_REQUEST_ACCESS_LOCATION = 100

    }
    private fun checkPermissions():Boolean{
        if(ActivityCompat.checkSelfPermission(this,
            android.Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == PERMISSION_REQUEST_ACCESS_LOCATION){
            if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED){

            }
            else{
                Toast.makeText(applicationContext,"Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if(isConnected){

                if(connectionMessages.getShowMain() === true){
                    builder.setMessage("Internet is back!")
                        .setPositiveButton("Refresh"){dialogInterface,it ->
                            connectionMessages.restartShowMain()

                            recreate()
                        }
                        .show()
                }

                binding.logoutBtn.isActivated = true


                builder = AlertDialog.Builder(this)

                firebaseAuth = FirebaseClient.getAuthInstance()
                var currentUser = firebaseAuth.currentUser
                var currentId = currentUser!!.uid




                val db = FirebaseClient.getDatabase()

                val ref = db.collection("clients").document(currentId)

                ref.get().addOnSuccessListener {
                    if (it != null) {

                        // To get the current values of data for the user
                        val currentName = it.data?.get("name").toString()
                        val currentEmail = it.data?.get("email").toString()
                        val currentStatus = it.data?.get("status").toString()
                        val currentPhoto = it.data?.get("photo").toString()

                        val user = User(currentId, currentName, currentEmail,currentStatus, currentPhoto )
                        UserPreferences.saveUserData(user)
                    }


                }

                getCurrentLocation()


                runBlocking {
                    launch(Dispatchers.IO) {




                        // Perform network operation here
                        val url ="https://api.openweathermap.org/data/2.5/weather?lat=4.601348&lon=-74.065659&units=metric&appid=82a483eb12ef0d04a79c4c35abd875af"
                        val resultJson = URL(url).readText()
                        Log.d("Weather", resultJson)
                        val jsonObj = JSONObject(resultJson)
                        val main = jsonObj.getJSONObject("main")
                        temp = main.getString("temp").toDouble()

                        temp =21.0




                    }


                }


            }
            else{
                if(connectionMessages.getShowMain() === false){
                    builder.setMessage("No Internet connection. You can see your profile or the menu list in the bottom navigation bar")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            //val intent = Intent(this, ProfileActivity::class.java)
                            //startActivity(intent)
                            connectionMessages.showMain()
                            binding.logoutBtn.setBackgroundColor(Color.GRAY)
                            dialogInterface.cancel()
                        }
                        .show()
                }


                binding.logoutBtn.isActivated = false


            }
        })
        temp =0.0
        /*runBlocking {
            launch(Dispatchers.IO){
                nearUniversity = checkNearUniversity()
            }
        }*/







    }

    private fun showDialogView() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.icecream_layout, null)

        val builder = AlertDialog.Builder(this)
            .setView(dialogView)

        val alertDialog = builder.show()

        var btn_okay = dialogView.findViewById(R.id.buttonOkay) as Button

        val txtIceCream = dialogView.findViewById(R.id.ice_cream_text) as TextView

        val maxSize = 34

        val scaledSizePixels = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            maxSize.toFloat(),
            resources.displayMetrics
        )

        txtIceCream.setTextSize(TypedValue.COMPLEX_UNIT_PX, scaledSizePixels)

        btn_okay.setOnClickListener{
            val intent = Intent(this, OrderActivity::class.java)
            startActivity(intent)
        }

        var btn_close = dialogView.findViewById(R.id.imageViewClose) as ImageView

        btn_close.setOnClickListener{
            alertDialog.cancel()
        }
    }

    private fun checkNearUniversity():Boolean{
        val minLat = 4.598206
        val maxLon = -74.060687
        val maxLat = 4.605309
        val minLon = -74.0705



        var check = latitude in minLat..maxLat && longitude in minLon..maxLon

        Log.d("Location", "$latitude, $longitude")
        Log.d("CheckNear", "$check")


        return latitude in minLat..maxLat && longitude in minLon..maxLon


    }

    override fun onDestroy() {
        super.onDestroy()
        Counter.restart()
    }


}