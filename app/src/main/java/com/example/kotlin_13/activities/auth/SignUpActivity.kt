package com.example.kotlin_13.activities.auth

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.kotlin_13.activities.MenuListLoginSignUpActivity
import com.example.kotlin_13.R
import com.example.kotlin_13.activities.connection.ConnectionMessageSingleton
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import com.example.kotlin_13.activities.data.FirebaseClient
import com.google.firebase.auth.FirebaseAuth
import com.example.kotlin_13.databinding.ActivitySignupBinding
import com.example.kotlin_13.model.repositories.UserRepository
import com.example.kotlin_13.viewmodels.UserViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignupBinding

    private lateinit var builder: AlertDialog.Builder

    private val userRepository = UserRepository()

    private val viewModel = UserViewModel(userRepository)

    private val connectionMessages= ConnectionMessageSingleton


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)

        builder = AlertDialog.Builder(this)

        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val networkConnectivityReceiver = NetworkConnectivityObserver(applicationContext)
        networkConnectivityReceiver.observe(this, androidx.lifecycle.Observer { isConnected ->
            if(isConnected){
                if(connectionMessages.getShowSignUp() === true){
                    builder.setMessage("Internet is back!")
                        .setPositiveButton("Refresh"){dialogInterface,it ->
                            connectionMessages.restartShowSignUp()
                            recreate()
                        }
                        .show()
                }

            }
            else{
                if(connectionMessages.getShowSignUp() === false){
                    builder.setMessage("No Internet connection. Register once the connection is back")
                        .setPositiveButton("OK"){dialogInterface,it ->
                            dialogInterface.cancel()
                        }
                        .show()
                    connectionMessages.showSignUp()

                }

                binding.btnSignup.isEnabled = false
                binding.btnSignup.setBackgroundColor(Color.GRAY)
            }
        })


        // Observa la propiedad isSignUpSuccessful del viewModel para saber si el registro fue exitoso
        viewModel.isSignUpSuccessful.observe(this) { isSuccess ->
            if (isSuccess) {
                // Redirects to signInActivity, but because it is logged, it goes to MainActivity
                val intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
            } else {
                // El registro falló, realiza alguna acción aquí
                builder.setMessage("Error creating the user")
                    .setPositiveButton("Cancel"){dialogInterface,it ->
                        dialogInterface.cancel()
                    }
                    .show()
            }
        }


        var birthday = ""


        // Date picker
        binding.btnSelectDate.setOnClickListener{
            val c = Calendar.getInstance()

            // on below line we are getting
            // our day, month and year.
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            // on below line we are creating a
            // variable for date picker dialog.
            val datePickerDialog = DatePickerDialog(
                // on below line we are passing context.
                this, R.style.DialogTheme,
                { view, year, monthOfYear, dayOfMonth ->
                    // on below line we are setting
                    // date to our text view.
                    birthday =
                        (year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth)
                    binding.txtDate.text=birthday


                },
                // on below line we are passing year, month
                // and day for the selected date in our date picker.
                year,
                month,
                day
            )

            // Max age 14 years old
            val todayDate = Calendar.getInstance()
            todayDate.add(Calendar.DATE, -5110)
            datePickerDialog.datePicker.maxDate = todayDate.timeInMillis;
            // at last we are calling show
            // to display our date picker dialog.
            datePickerDialog.show()


        }
        // Button to go to SignInActivity
        binding.loginButton.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }

        binding.menus.setOnClickListener {
            val intent = Intent(this, MenuListLoginSignUpActivity::class.java)
            startActivity(intent)
        }

        // minLength in name
        val editText = binding.nameET
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // do nothing
            }

            override fun afterTextChanged(s: Editable) {
                if (s.length < 2) {
                    editText.error = "Input must be at least 2 characters"
                }
            }
        })

        // Button to register a new user when all the fields are with data
        binding.btnSignup.setOnClickListener {


            val name = binding.nameET.text.toString()
            val student = binding.Student.isChecked
            val staff = binding.Staff.isChecked
            val email = binding.emailEt.text.toString()
            val password = binding.passET.text.toString()
            val confirmPass = binding.passConfirmET.text.toString()
            var status = ""

            // To set a String variable to show the status, if student or staff box is checked
            if(student){
                status = "Student/Professor"
            }
            else if(staff){
                status = "Staff"
            }


            viewModel.signUp(name,email,password,confirmPass, status, birthday,builder)

        }
    }




}