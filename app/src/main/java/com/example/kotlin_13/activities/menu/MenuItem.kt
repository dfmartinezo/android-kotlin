package com.example.kotlin_13.activities.menu

data class MenuItem(
    val name: String = "",
    val price: Int = 0,
    val description: String = "",
    val photoURL: String = ""
): java.io.Serializable{
    constructor() : this("", 0, "", "")
}
