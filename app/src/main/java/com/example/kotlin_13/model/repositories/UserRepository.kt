package com.example.kotlin_13.model.repositories

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import com.example.kotlin_13.activities.connection.NetworkConnectivityObserver
import com.example.kotlin_13.activities.data.FirebaseClient
import com.google.firebase.auth.ktx.userProfileChangeRequest

class UserRepository {
    private val firebaseAuth = FirebaseClient.getAuthInstance()
    private val db = FirebaseClient.getDatabase()

    fun createUser(
        name: String,
        email: String,
        password: String,
        status: String,
        birthday: String,
        builder: AlertDialog.Builder,
        callback: (Boolean) -> Unit
    ) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { authTask ->
            if (authTask.isSuccessful) {
                val user = firebaseAuth.currentUser
                val profileUpdate = userProfileChangeRequest {
                    displayName = name
                    photoUri = Uri.parse("https://thumbs.dreamstime.com/b/default-profile-picture-avatar-photo-placeholder-vector-illustration-default-profile-picture-avatar-photo-placeholder-vector-189495158.jpg")
                }
                user?.updateProfile(profileUpdate)?.addOnCompleteListener { profileTask ->
                    if (profileTask.isSuccessful) {
                        val userMap = hashMapOf(
                            "name" to name,
                            "password" to password,
                            "email" to email,
                            "status" to status,
                            "photo" to "https://thumbs.dreamstime.com/b/default-profile-picture-avatar-photo-placeholder-vector-illustration-default-profile-picture-avatar-photo-placeholder-vector-189495158.jpg",
                            "birthday" to birthday,
                            "likedMenusRef" to null,
                            "methodsRefs" to null,
                            "ordersRef" to null,
                            "ref" to null,
                        )
                        val uid = user.uid
                        db.collection("clients").document(uid).set(userMap).addOnCompleteListener { dbTask ->
                            callback(dbTask.isSuccessful)
                        }
                    } else {
                        callback(false)
                    }
                }
            } else {
                callback(false)
            }
        }
    }

    fun findUser(email: String, pass: String, builder: AlertDialog.Builder, callback: (Boolean) -> Unit) {

        // Sign in method from auth Firebase
        firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener {

            // If correct credentials, goes to MainActivity
            if (it.isSuccessful) {
                callback(it.isSuccessful)
            }

            // If not, shows error
            else {
                callback(false)

            }
        }
    }
}