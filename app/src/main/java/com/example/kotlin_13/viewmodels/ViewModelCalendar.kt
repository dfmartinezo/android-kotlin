package com.example.kotlin_13.viewmodels


import androidx.lifecycle.ViewModel


import android.app.Activity
import android.content.Intent
import com.example.kotlin_13.activities.PaymentActivity
import com.example.kotlin_13.model.repositories.CalendarRepository

import java.util.Calendar


class ViewModelCalendar : ViewModel () {

    private val repository = CalendarRepository()

    fun realizarPago(calendar: Calendar) {
        repository.realizarPago(calendar)
    }


    fun volverAPaymentActivity(activity: Activity) {
        val intent = Intent(activity, PaymentActivity::class.java)
        activity.startActivity(intent)
    }
}

