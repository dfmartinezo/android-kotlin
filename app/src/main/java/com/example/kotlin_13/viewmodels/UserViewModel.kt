package com.example.kotlin_13.viewmodels

import android.app.AlertDialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlin_13.model.repositories.UserRepository

class UserViewModel(    private val userRepository: UserRepository
) {


    private val _isSignUpSuccessful = MutableLiveData<Boolean>()
    val isSignUpSuccessful: LiveData<Boolean>
        get() = _isSignUpSuccessful

    private val _isSignInSuccessful = MutableLiveData<Boolean>()
    val isSignInSuccessful: LiveData<Boolean>
        get() = _isSignInSuccessful




    fun signUp(
        name: String,
        email: String,
        password: String,
        confirmPass: String,
        status: String,
        birthday: String,
        builder: AlertDialog.Builder
    ) {

        if (email.isNotEmpty() && password.isNotEmpty() && confirmPass.isNotEmpty() && name.isNotEmpty() && birthday != "" && status.isNotEmpty()) {
            if (password == confirmPass) {
                userRepository.createUser(
                    name,
                    email,
                    password,
                    status,
                    birthday,
                    builder
                ) { isSuccess ->
                    _isSignUpSuccessful.value = isSuccess
                }
            } else {
                builder.setMessage("Passwords don't match")
                    .setPositiveButton("Cancel") { dialogInterface, it ->
                        dialogInterface.cancel()
                    }
                    .show()


            }
        }
        else{
            builder.setMessage("Empty fields are not allowed")
                .setPositiveButton("OK") { dialogInterface, it ->
                    dialogInterface.cancel()
                }
                .show()
        }

    }

    fun signIn(email: String, pass: String, builder: AlertDialog.Builder) {

        if (email.isNotEmpty() && pass.isNotEmpty()) {

            userRepository.findUser(
                email,
                pass,
                builder
            ) { isSuccess ->
                _isSignInSuccessful.value = isSuccess
            }

        }

        // Case there are empty fields
        else {
            builder.setMessage("Empty Fields are not allowed")
                .setPositiveButton("OK"){dialogInterface,it ->
                    dialogInterface.cancel()
                }
                .show()
        }

    }



}